# Foosball league
Classifica dei giocatori di calciobalilla di TAG Fondazione Agnelli

## Getting Started

```
git clone git@bitbucket.org:ldv00/foosball-league-backend.git
```

### Prerequisites

* Nodejs v8.4.0

### Installing

Install npm modules

```
npm install
```

###Environment setup
Create **logs**, and **env** folders in the project root.

Inside **env** folder create a config file named like one of the environments in config/server.config.js:

* es. development.js

#### development.js
```
const
    port = process.env.PORT || 80,
    baseUrl = 'http://foosball-league.dev',
    apiVersion = 'api/v1';

module.exports = {
    appName: 'foosball-league',
    secret: '<YOUR-KEY>',
    tokenSecret: '<YOUR-KEY>',
    sessionSecret: '<YOUR-KEY>',
    database: {
        host: 'localhost',
        dialect: 'mysql',
        port: 3306,
        encrypt: false,
        name: 'foosball-league',
        username: '<YOUR-USERNAME>',
        password: '<YOUR-PASSWORD>'
    },
    baseUrl: baseUrl,
    apiVersion: apiVersion,
    port: port,
    endPoint: baseUrl + ':' + port + '/' + apiVersion + '/',
    nodeMailer: {
        templatesPath: 'views/emails/',
        service: 'mx.geobox.eu',
        email: 'noreply@foosball-league.com',
        senderEmail: 'noreply@foosball-league.com',
        password: ''
    },
    facebookLogin: {
        'clientID': '',
        'clientSecret': '',
        'callbackURL': '',
        'profileFields': ['id', 'email', 'birthday', 'gender', 'link', 'locale', 'name', 'photos']
    },
    logsPath: 'logs/',
    whitelist: ['']
};
```

##Running in local
Install PM2 globally and run npm start
```
npm i -g pm2 && npm run start:dev
```

## Running the tests

There aren't any test yet.

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Deploy on [Private Server]()

## Built With

* [Nodejs](https://nodejs.org/it/) - The back-end framework used
* [Express](http://expressjs.com/it/) - The back-end framework used
* [Sequelize](http://docs.sequelizejs.com/en/v3/) -  The promise-based ORM for Node.js
* [Passport](http://passportjs.org/) - Used to manage login and signup
* [Nodemailer](https://github.com/nodemailer/nodemailer) - Used to send emails and newsletter
* [Lodash](https://lodash.com/) - Used to manipulate arrays

## Versioning

[Bitbucket](https://bitbucket.org/ldv00/foosball-league-backend). For the versions available, see the **tags on this repository**. 

## Authors

**Ludovico Dellavecchia**

See also the list of [CONTRIBUTORS.md](CONTRIBUTORS.md) who participated in this project.

## License

**All rights reserved**
This project is licensed under the Copyright License - see the [LICENSE.md](LICENSE.md) file for details.
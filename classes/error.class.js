"use strict";
const _ = require('lodash');
const error = class Error {
    constructor(e) {
        this._stack = process.env.NODE_ENV !== 'production' ? (e.message ? JSON.stringify(e.message) : e.stack) : '';
        this._status = e.status;
        if (e.errors && e.errors.length) {
            this._status = 400;
            this._stack = '';
            _.forEach(e.errors, (error, index) => {
                this._stack = this._stack + error.message + (index === e.errors.length - 1 ? '' : ' - ');
            })
        }
    }

    stack() {
        if (!this._stack) {
            switch (this.status()) {
                case 400:
                    this._stack = 'HTTP_BAD_REQUEST';
                    break;
                case 401:
                    this._stack = 'HTTP_UNAUTHORIZED';
                    break;
                case 402:
                    this._stack = 'HTTP_PAYMENT_REQUIRED';
                    break;
                case 403:
                    this._stack = 'HTTP_FORBIDDEN';
                    break;
                case 404:
                    this._stack = 'HTTP_NOT_FOUND';
                    break;
                case 409:
                    this._stack = 'HTTP_CONFLICT';
                    break;
                case 500:
                    this._stack = 'HTTP_INTERNAL_SERVER_ERROR';
                    break;
                default:
                    this._stack = this._stack ? this._stack : 'HTTP_INTERNAL_SERVER_ERROR';
                    break;
            }
        }
        return this._stack;
    };

    status() {
        return this._status ? this._status : 500;
    };
};
module.exports = error;
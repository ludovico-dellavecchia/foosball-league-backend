"use strict";
const
    Stat = {},
    db = require('../../models/index');

Stat.get = async (req) => {
    if (req.params.id) {
        const stat = await db.stat.findById(req.params.id);
        if(!stat){
            throw ({
                status: 404,
                stack: 'STAT_NOT_FOUND'
            });
        }
        return stat;
    }else{
        return await db.stat.findAll({
            where: req.query
        });
    }
};

Stat.insert = async (stat) => {
    return await db.stat.create(stat);
};

Stat.update = async (stat) => {
    if(!stat.id){
        throw ({
            status: 400,
            stack: 'MISSING_STAT_ID'
        });
    }
    if(!await db.stat.findById(stat.id)){
        throw ({
            status: 404,
            stack: 'STAT_NOT_FOUND'
        });
    }
    return await db.stat.update(stat, {
        where: {
            id: stat.id
        }
    });
};


Stat.delete = async (req) => {
    if(!req.params.id){
        throw ({
            status: 400,
            stack: 'MISSING_STAT_ID'
        });
    }
    if(!await db.stat.findById(req.params.id)){
        throw ({
            status: 404,
            stack: 'STAT_NOT_FOUND'
        });
    }
    return await db.stat.destroy({
        where: {
            id: req.params.id
        }
    });
};


module.exports = Stat;
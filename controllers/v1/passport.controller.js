// load all the things we need
const
    LocalStrategy = require('passport-local').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    config = require('../../config/server.config'),
    db = require('../../models/index'),
    User = require('./user.controller'),
    Email = require('./email.controller');

// expose this function to our app using module.exports
module.exports = (passport) => {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser((user, done) => {
        console.log('serializeUser');
        done(null, user.id);
    });

    passport.deserializeUser(async (id, done) => {
        console.log('deserializeUser');
        db.user.findById(id).then((user) => {
            user = user.display();
            done(null, user);
        }).catch((err) => {
            done(err);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-signup', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        async (req, email, password, next) => {
            console.log('Passport local sign up ');
            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            try {
                const user = await User.insert({
                    email: req.body.email,
                    password: req.body.password,
                    termsAndConditions: req.body.termsAndConditions,
                    privacy: req.body.privacy,
                    birthDate: req.body.birthDate,
                    birthPlace: req.body.birthPlace,
                    fiscalCode: req.body.fiscalCode,
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    phone: req.body.phone,
                    sex: req.body.sex,
                    smartPhone: req.body.smartPhone,
                    vatNumber: req.body.vatNumber
                });
                await Email.send({
                    template: 'sign-up',
                    emails: user.email,
                    subject: 'Complete registration on Sigascot',
                    values: [
                        {
                            key: '**email**',
                            value: user.email
                        },
                        {
                            key: '**endPoint**',
                            value: config.endPoint + 'confirm-email'
                        },
                        {
                            key: '**activationToken**',
                            value: user.activationToken
                        }
                    ]
                });
                next(null, user.display());
            } catch (e) {
                next(e);
            }
        })
    );


    // =========================================================================
    // LOCAL LOGIN ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-login', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true, // allows us to pass back the entire request to the callback
            failureMessage: "Invalid username or password"
        },
        async (req, email, password, next) => { // callback with email and password from our form
            try {
                // find a user whose email is the same as the forms email
                // we are checking to see if the user trying to login already exists
                const user = await db.user.findOne({
                    where: {
                        email: email
                    }
                });
                if (!user) {
                    return next({
                        stack: 'INVALID_CREDENTIALS',
                        status: 401
                    });
                }
                const authorized = await user.isAuthorized(password);
                if (!authorized) {
                    return next({
                        stack: 'INVALID_CREDENTIALS',
                        status: 401
                    });
                } else if (!user.active) {
                    return next({
                        stack: 'USER_NOT_ACTIVE',
                        status: 403
                    });
                }
                await req.logIn(user, async (err) => {
                    if (err) {
                        return next({
                            stack: 'HTTP_INTERNAL_SERVER_ERROR',
                            status: 500
                        });
                    }
                    return next(null, await user.display());
                });
            } catch (e) {
                next(e);
            }
        })
    );

    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use('facebook', new FacebookStrategy({
            // pull in our app id and secret from our config.js file
            display: config.facebookLogin.display,
            clientID: config.facebookLogin.clientID,
            clientSecret: config.facebookLogin.clientSecret,
            callbackURL: config.facebookLogin.callbackURL,
            profileFields: config.facebookLogin.profileFields
        },
        // facebook will send back the token and profile
        (token, refreshToken, profile, done) => {
            //console.log('Facebook data:', profile);
            // asynchronous
            process.nextTick(() => {
                // find the user in the database based on their facebook id
                db.user.find({
                    where: {
                        'facebook_id': profile.id
                    },
                    raw: true
                }).then((user) => {
                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    // if the user is found, then log them in
                    if (user) {
                        return done(null, user); // user found, return that user
                    } else {

                        if (profile.emails) {

                            db.user.find({
                                where: {'email': profile.emails[0].value},
                                raw: true
                            }).then((user) => {
                                if (user) {
                                    return done(null, user); // user found, return that user
                                } else {
                                    // if there is no user found with that facebook id, create them
                                    user = {
                                        name: profile.name.givenName,
                                        surname: profile.name.familyName,
                                        email: profile.emails ? profile.emails[0].value : null,
                                        facebook_id: profile.id,
                                        username: profile.name.givenName,
                                        password: null,
                                        admin: 0,
                                        year_of_birth: profile._json ? (profile._json.birthday ? profile._json.birthday.split('/')[2] : null) : null,
                                        picture_url: profile.photos ? profile.photos[0].value : 'assets/img/user-icon.jpg'
                                    };
                                    // save our user to the database
                                    db.user.create(user, {
                                        raw: true
                                    }).then((user) => {
                                        console.log(user);
                                        //TODO: welcome email with activation code
                                        return done(null, user);
                                    }, (err) => {
                                        return done(err);
                                    });
                                }
                            });
                        } else {
                            user = {
                                name: profile.name.givenName,
                                surname: profile.name.familyName,
                                email: profile.emails ? profile.emails[0].value : null,
                                facebook_id: profile.id,
                                username: profile.name.givenName,
                                password: null,
                                admin: 0,
                                year_of_birth: profile._json ? (profile._json.birthday ? profile._json.birthday.split('/')[2] : null) : null,
                                picture_url: profile.photos ? profile.photos[0].value : 'assets/img/user-icon.jpg'
                            };
                            // save our user to the database
                            db.user.create(user, {
                                raw: true
                            }).then((user) => {
                                console.log(user);
                                //TODO: welcome email with activation code
                                return done(null, user);
                            }, (err) => {
                                return done(err);
                            });
                        }
                    }
                }, (err) => {
                    return done(err);
                });
            });

        })
    );
};
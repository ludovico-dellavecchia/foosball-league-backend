"use strict";
const
    Invitation = {},
    db = require('../../models/index');

Invitation.get = async (req) => {
    if (req.params.id) {
        const invitation = await db.invitation.findById(req.params.id);
        if(!invitation){
            throw ({
                status: 404,
                stack: 'INVITATION_NOT_FOUND'
            });
        }
        return invitation;
    }else{
        return await db.invitation.findAll({
            where: req.query
        });
    }
};

Invitation.insert = async (invitation) => {
    return await db.invitation.create(invitation);
};

Invitation.update = async (invitation) => {
    if(!invitation.id){
        throw ({
            status: 400,
            stack: 'MISSING_INVITATION_ID'
        });
    }
    if(!await db.invitation.findById(invitation.id)){
        throw ({
            status: 404,
            stack: 'INVITATION_NOT_FOUND'
        });
    }
    return await db.invitation.update(invitation, {
        where: {
            id: invitation.id
        }
    });
};


Invitation.delete = async (req) => {
    if(!req.params.id){
        throw ({
            status: 400,
            stack: 'MISSING_INVITATION_ID'
        });
    }
    if(!await db.invitation.findById(req.params.id)){
        throw ({
            status: 404,
            stack: 'INVITATION_NOT_FOUND'
        });
    }
    return await db.invitation.destroy({
        where: {
            id: req.params.id
        }
    });
};


module.exports = Invitation;
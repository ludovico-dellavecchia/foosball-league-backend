"use strict";
const
    Game = {},
    db = require('../../models/index');

Game.get = async (req) => {
    if (req.params.id) {
        const game = await db.game.findById(req.params.id);
        if(!game){
            throw ({
                status: 404,
                stack: 'GAME_NOT_FOUND'
            });
        }
        return game;
    }else{
        return await db.game.findAll({
            where: req.query
        });
    }
};

Game.insert = async (game) => {
    return await db.game.create(game);
};

Game.update = async (game) => {
    if(!game.id){
        throw ({
            status: 400,
            stack: 'MISSING_GAME_ID'
        });
    }
    if(!await db.game.findById(game.id)){
        throw ({
            status: 404,
            stack: 'GAME_NOT_FOUND'
        });
    }
    return await db.game.update(game, {
        where: {
            id: game.id
        }
    });
};


Game.delete = async (req) => {
    if(!req.params.id){
        throw ({
            status: 400,
            stack: 'MISSING_GAME_ID'
        });
    }
    if(!await db.game.findById(req.params.id)){
        throw ({
            status: 404,
            stack: 'GAME_NOT_FOUND'
        });
    }
    return await db.game.destroy({
        where: {
            id: req.params.id
        }
    });
};


module.exports = Game;
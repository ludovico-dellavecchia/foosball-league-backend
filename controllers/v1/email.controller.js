"use strict";
const
    fs = require('fs'),
    path = require('path'),
    _ = require('lodash'),
    config = require('../../config/server.config'),
    nodemailer = require('nodemailer'),
    email = {},
    stream = fs.createWriteStream(path.join(config.logsPath, 'nodemailer.log'), {flags: 'a'}),
    transporter = nodemailer.createTransport({
        host: config.nodeMailer.service,
        secure: true,
        auth: {
            user: config.nodeMailer.email,
            pass: config.nodeMailer.password
        }
    });

email.send = async (options) => {
    let html;
    if (options.template) {
        html = fs.readFileSync(config.nodeMailer.templatesPath + options.template + ".html", 'utf8');
        _.forEach(options.values, (value) => {
            html = html.replace(value.key, value.value);
        });
    } else if (options.html) {
        html = options.html
    }
    let info = await transporter.sendMail({
        from: config.nodeMailer.senderEmail, // sender address
        to: options.emails, // list of receivers
        subject: options.subject, // Subject line
        html: html // html body
    });
    info = JSON.stringify(info);
    info = info.concat('\n');
    await stream.write(info);
};

module.exports = email;
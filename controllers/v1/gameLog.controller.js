"use strict";
const
    GameLog = {},
    db = require('../../models/index');

GameLog.get = async (req) => {
    if (req.params.id) {
        const gameLog = await db.gameLog.findById(req.params.id);
        if(!gameLog){
            throw ({
                status: 404,
                stack: 'GAME_LOG_NOT_FOUND'
            });
        }
        return gameLog;
    }else{
        return await db.gameLog.findAll({
            where: req.query
        });
    }
};

GameLog.insert = async (gameLog) => {
    return await db.gameLog.create(gameLog);
};

GameLog.update = async (gameLog) => {
    if(!gameLog.id){
        throw ({
            status: 400,
            stack: 'MISSING_GAME_LOG_ID'
        });
    }
    if(!await db.gameLog.findById(gameLog.id)){
        throw ({
            status: 404,
            stack: 'GAME_LOG_NOT_FOUND'
        });
    }
    return await db.gameLog.update(gameLog, {
        where: {
            id: gameLog.id
        }
    });
};


GameLog.delete = async (req) => {
    if(!req.params.id){
        throw ({
            status: 400,
            stack: 'MISSING_GAME_LOG_ID'
        });
    }
    if(!await db.gameLog.findById(req.params.id)){
        throw ({
            status: 404,
            stack: 'GAME_LOG_NOT_FOUND'
        });
    }
    return await db.gameLog.destroy({
        where: {
            id: req.params.id
        }
    });
};


module.exports = GameLog;
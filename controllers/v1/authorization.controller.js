"use strict";
const
    authorization = {},
    config = require('../../config/server.config'),
    User = require('./user.controller'),
    Email = require('./email.controller'),
    db = require('../../models/index'),
    randToken = require("rand-token").generator({
        chars: 'base32'
    });

authorization.requireLogin = (req, res, next) => {
    //TODO: enable auth check
    //if (req.isAuthenticated()) {
        return next();
    //}
    return res.sendStatus(401);
};

authorization.requireAdmin = (req, res, next) => {
    if (req.isAuthenticated() && req.user.admin === true) {
        return next();
    }
    return res.sendStatus(403);
};

authorization.signup = async (user) => {
    user = await User.insert(user);
    await Email.send({
        template: 'sign-up',
        emails: user.email,
        subject: 'Complete registration on Sigascot',
        values: [
            {
                key: '**endPoint**',
                value: config.endPoint
            },
            {
                key: '**activationToken**',
                value: user.activationToken
            }
        ]
    });
};

authorization.login = async (candidateUser) => {
    const user = await db.user.findOne({
        where: {
            email: candidateUser.email
        }
    });
    if (!user) {
        throw ({
            stack: 'INVALID_CREDENTIALS',
            status: 401
        });
    }
    const authorized = await user.isAuthorized(candidateUser.password);
    if (!authorized) {
        throw ({
            stack: 'INVALID_CREDENTIALS',
            status: 401
        });
    } else if (!user.active) {
        throw ({
            stack: 'USER_NOT_ACTIVE',
            status: 403
        });
    }
    return user.getToken();

};

authorization.resetPassword = async (req) => {
    if (req.params.token) {
        const token = req.params.token;
        const user = await db.user.find({
            where: {
                resetToken: token
            }
        });
        if (!user) {
            throw ({
                status: 403,
                stack: 'INVALID_TOKEN'
            });
        }
        if (!req.body.password) {
            throw ({
                status: 400,
                stack: 'MISSING_NEW_PASSWORD'
            })
        }
        let hashPassword = await db.user.beforeUpdate({
            password: req.body.password
        });
        hashPassword.resetToken = null;
        await user.update(hashPassword);
    } else {
        if (!req.body.email) {
            throw ({
                status: 400,
                stack: 'MISSING_EMAIL'
            })
        }
        const token = randToken.generate(22);
        const user = await db.user.find({
            where: {
                email: req.body.email
            }
        });
        if(!user){
            return;
        }
        await user.update({
            resetToken: token
        });
        await Email.send({
            template: 'reset-password',
            emails: user.email,
            subject: 'Reset your password',
            values: [
                {
                    key: '**firstName**',
                    value: user.firstName
                },
                {
                    key: '**lastName**',
                    value: user.lastName
                },
                {
                    key: '**endPoint**',
                    value: config.endPoint + 'reset-password'
                },
                {
                    key: '**token**',
                    value: token
                }
            ]
        });
    }
};

authorization.confirmEmail = async (req) => {
    const user = await db.user.update({
        active: true
    }, {
        where: {
            activationToken: req.params.activationToken
        }
    });
    if (user[0] === 0) {
        throw ({
            status: 401,
            stack: 'INVALID_TOKEN'
        });
    }
};

authorization.updateEmail = async (req) => {
    if (req.params.token) {
        const token = req.params.token.toUpperCase();
        const userEmail = await db.userEmail.find({
            where: {
                token: token,
                userId: req.user.id,
                validate: false
            }
        });
        if (!userEmail) {
            throw ({
                status: 403,
                stack: 'INVALID_TOKEN'
            });
        }

        const user = await db.user.findById(userEmail.id);
        await user.update({
            email: userEmail.email
        });
        await await db.userEmail.update({
            validate: true
        }, {
            where: {
                token: token,
                userId: req.user.id
            }
        });
    } else {
        if (!req.body.email) {
            throw ({
                status: 400,
                stack: 'MISSING_EMAIL'
            })
        }
        const duplicateEmail = await db.user.find({
            where: {
                email: req.body.email
            },
            attributes: ['email']
        });
        if (duplicateEmail) {
            throw ({
                status: 409,
                stack: 'EMAIL_ALREADY_EXIST'
            })
        }
        if (!req.body.password) {
            throw ({
                status: 400,
                stack: 'MISSING_PASSWORD'
            })
        }
        const token = randToken.generate(6);
        const user = await db.user.findById(req.user.id);
        const authorized = await user.isAuthorized(req.body.password);
        if (!authorized) {
            throw ({
                status: 403,
                stack: 'INVALID_PASSWORD'
            });
        }
        await user.createUserEmail({
            email: req.body.email,
            token: token
        });
        await Email.send({
            template: 'update-email',
            emails: req.body.email,
            subject: 'Confirm new email',
            values: [
                {
                    key: '**firstName**',
                    value: user.firstName
                },
                {
                    key: '**lastName**',
                    value: user.lastName
                },
                {
                    key: '**oldEmail**',
                    value: user.email
                },
                {
                    key: '**newEmail**',
                    value: req.body.email
                },
                {
                    key: '**endPoint**',
                    value: config.endPoint + 'update-email'
                },
                {
                    key: '**token**',
                    value: token
                }
            ]
        });
    }
};


module.exports = authorization;
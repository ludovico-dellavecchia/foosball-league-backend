"use strict";
const
    User = {},
    _ = require('lodash'),
    db = require('../../models/index');

User.get = async (req) => {
    let include = [];
    if (req.query.include) {
        include = req.query.include.split(',');
        delete req.query.include;
    }
    if (req.params.id) {
        if (!req.user.admin) {
            req.params.id = req.user.id;
        }
        const user = await db.user.find({
            where: {
                id: req.params.id
            },
            include: include
        });
        if (!user) {
            throw ({
                status: 404,
                stack: 'USER_NOT_FOUND'
            });
        }
        return await user.display();
    } else {
        let users = await db.user.findAll({
            where: req.query,
            include: include
        });
        users = _.map(users, (user) => {
            return user.display();
        });
        return users;
    }
};

User.insert = async (user) => {
    user = await db.user.beforeCreate(user);
    user = await db.user.create(user);
    return user;
};

User.update = async (req) => {
    let user = req.body.user;
    //If user isn't admin set user id equals to authenticated user id
    if (!req.user.admin) {
        user.id = req.user.id;
    }
    if (!user.id) {
        throw ({
            status: 400,
            stack: 'MISSING_USER_ID'
        })
    }
    if (!await db.user.findById(user.id)) {
        throw ({
            status: 404,
            stack: 'USER_NOT_FOUND'
        })
    }
    user = await db.user.beforeUpdate(user);
    user = await db.user.update(user, {
        where: {
            id: user.id
        }
    });
    return user;
};


User.delete = async (req) => {
    return await db.user.destroy({
        where: {
            id: req.user.admin ? req.param.id : req.user.id
        }
    });
};

User.getSubscription = async (req) => {
    const user = await db.user.findById(req.user.admin ? req.params.id : req.user.id);
    if (!user) {
        throw ({
            status: 404,
            stack: 'USER_NOT_FOUND'
        });
    }
    return await user.getSubscriptions();
};

User.setSubscription = async (req) => {
    const user = await db.user.findById(req.user.admin ? req.params.id : req.user.id);
    if (!user) {
        throw ({
            status: 404,
            stack: 'USER_NOT_FOUND'
        });
    }
    const subscription = req.body.subscription;
    if (!subscription.productId) {
        throw ({
            status: 400,
            stack: 'MISSING_PRODUCT_ID'
        });
    }
    const product = await db.product.findById(subscription.productId);
    if(!product){
        throw ({
            status: 404,
            stack: 'PRODUCT_NOT_FOUND'
        });
    }
    return await user.createSubscription(subscription);
};

User.removeSubscription = async (req) => {
    const user = await db.user.findById(req.user.admin ? req.params.id : req.user.id);
    if (!user) {
        throw ({
            status: 404,
            stack: 'USER_NOT_FOUND'
        });
    }
    const subscription = await user.getSubscriptions({
        where: req.body.subscription
    });
    return await user.removeSubscription(subscription);
};

User.getAddress = async (req) => {
    const user = await db.user.findById(req.user.admin ? req.params.id : req.user.id);
    if (!user) {
        throw ({
            status: 404,
            stack: 'USER_NOT_FOUND'
        });
    }
    return await user.getUserAddresses();
};

User.setAddress = async (req) => {
    const user = await db.user.findById(req.user.admin ? req.params.id : req.user.id);
    if (!user) {
        throw ({
            status: 404,
            stack: 'USER_NOT_FOUND'
        });
    }
    const address = req.body.address;
    return await user.createUserAddress(address);
};

User.removeAddress = async (req) => {
    const user = await db.user.findById(req.user.admin ? req.params.id : req.user.id);
    if (!user) {
        throw ({
            status: 404,
            stack: 'USER_NOT_FOUND'
        });
    }
    const address = await user.getUserAddresses({
        where: req.body.address
    });
    return await user.removeUserAddress(address);
};

module.exports = User;
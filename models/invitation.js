'use strict';
module.exports = (sequelize, DataTypes) => {
    return sequelize.define('invitation', {
            status: {
                type: DataTypes.ENUM(['pending', 'accepted', 'refused']),
                allowNull: false,
                defaultValue: 'pending'
            }
        }, {
            tableName: 'invitation',
            paranoid: true
        }
    );
};

'use strict';
module.exports = (sequelize, DataTypes) => {
    return sequelize.define('stat', {
            played: {
                type: DataTypes.INTEGER,
                allowNull: false,
                defaultValue: 0
            },
            won: {
                type: DataTypes.INTEGER,
                allowNull: false,
                defaultValue: 0
            },
            lost: {
                type: DataTypes.INTEGER,
                allowNull: false,
                defaultValue: 0
            },
            points: {
                type: DataTypes.FLOAT,
                allowNull: false,
                defaultValue: 0,
                comment: 'All points'
            },
            pointsM0: {
                type: DataTypes.FLOAT,
                allowNull: false,
                defaultValue: 0,
                comment: 'Current month points'
            },
            pointsM1: {
                type: DataTypes.FLOAT,
                allowNull: false,
                defaultValue: 0,
                comment: 'Last month points'
            },
            pointsM2: {
                type: DataTypes.FLOAT,
                allowNull: false,
                defaultValue: 0,
                comment: 'Two month ago points'
            },
            lastPlayed: {
                type: DataTypes.DATE,
                allowNull: true
            },
            lastWon: {
                type: DataTypes.DATE,
                allowNull: true
            },
            lastLost: {
                type: DataTypes.DATE,
                allowNull: true
            }

        }, {
            tableName: 'stats',
            paranoid: true
        }
    );
};

"use strict";
const
    config = require('../config/server.config'),
    fs = require('fs'),
    path = require('path'),
    lodash = require('lodash'),
    Sequelize = require('sequelize'),
    sequelize = new Sequelize(config.database.name, config.database.username, config.database.password, {
        host: config.database.host,
        dialect: config.database.dialect, // or 'sqlite', 'postgres', 'mariadb'
        port: config.database.port, // or 5432 (for postgres)
        timezone: '+01:00', // Forcing the timezone to Italian UTC,
        dialectOptions: {
            encrypt: config.database.encrypt
        }
    }), db = {};

fs
    .readdirSync(__dirname)
    .filter((file) => {
        return (file.indexOf('.') !== 0) && (file !== 'index.js') && (file !== 'node_modules');
    })
    .forEach((file) => {
        const model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach((modelName) => {
    if ('associate' in db[modelName]) {
        db[modelName].associate(db);
    }
});

module.exports = lodash.extend({
    sequelize: sequelize,
    Sequelize: Sequelize
}, db);

/* ===== User =====*/
db.user.hasOne(db.stat, {
    foreignKey: 'userId',
    as: 'user'
});

db.stat.belongsTo(db.user, {
    foreignKey: 'userId',
    as: 'user'
});

/* ===== Invitation =====*/
db.invitation.belongsTo(
    db.user, {
        foreignKey: 'fromUser',
        as: 'from'
    }
);

db.invitation.belongsTo(
    db.user, {
        foreignKey: 'toUser',
        as: 'to'
    }
);


/* =====  Game =====*/
db.game.belongsTo(db.user, {
    foreignKey: 'userCreator',
    as: 'gameCreator'
});

db.game.belongsTo(db.user, {
    foreignKey: 'user1',
    as: 'player1'
});

db.game.belongsTo(db.user, {
    foreignKey: 'user2',
    as: 'player2'
});

db.game.belongsTo(db.user, {
    foreignKey: 'user3',
    as: 'player3'
});

db.game.belongsTo(db.user, {
    foreignKey: 'user4',
    as: 'player4'
});

db.game.belongsTo(db.gameType, {
    foreignKey: 'gameTypeId',
    as: 'type'
});

/* =====  Game Log =====*/
db.game.hasMany(db.gameLog, {
    foreignKey: 'gameId'
});

db.gameLog.belongsTo(db.user, {
    foreignKey: 'userRed1'
});

db.gameLog.belongsTo(db.user, {
    foreignKey: 'userRed2'
});

db.gameLog.belongsTo(db.user, {
    foreignKey: 'userBlue1'
});

db.gameLog.belongsTo(db.user, {
    foreignKey: 'userBlue2'
});
'use strict';
const
    bcrypt = require('bcryptjs'),
    randToken = require('rand-token'),
    config = require('../config/server.config'),
    jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
    let User = sequelize.define('user', {
            activationToken: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true
            },
            active: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            admin: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            email: {
                type: DataTypes.STRING,
                unique: true,
                allowNull: false,
                isEmail: true
            },
            birthDate: {
                type: DataTypes.DATEONLY,
                isDate: true
            },
            birthPlace: {
                type: DataTypes.STRING
            },
            fiscalCode: {
                type: DataTypes.STRING,
                unique: true,
                len: [16],
                isAlphanumeric: true,
            },
            firstName: {
                type: DataTypes.STRING
            },
            lastName: {
                type: DataTypes.STRING
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false
            },
            phone: {
                type: DataTypes.STRING,
                isNumeric: true
            },
            privacy: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            resetToken: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null
            },
            gender: {
                type: DataTypes.ENUM(['man', 'woman', 'other'])
            },
            smartPhone: {
                type: DataTypes.STRING,
                isNumeric: true
            },
            termsAndConditions: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            vatNumber: {
                type: DataTypes.STRING,
                len: [11],
                isAlphanumeric: true

            }
        }, {
            tableName: 'user',
            paranoid: true
        }
    );


    User.prototype.display = function () {
        return {
            admin: this.admin,
            birthDate: this.birthDate,
            birthPlace: this.birthPlace,
            fiscalCode: this.fiscalCode,
            email: this.email,
            id: this.id,
            firstName: this.firstName,
            lastName: this.lastName,
            fullName: this.firstName ? this.firstName + ' ' + this.lastName : undefined,
            phone: this.phone,
            sex: this.sex,
            smartPhone: this.smartPhone,
            vatNumber: this.vatNumber,
            subscriptions: this.subscriptions
        };
    };

    User.prototype.getToken = async function () {
        try {
            return await jwt.sign({
                    user: await this.display()
                },
                config.tokenSecret,
                {
                    expiresIn: '24h'
                }
            );
        } catch (e) {
            console.error(e);
            throw e;
        }
    };

    User.prototype.isAuthorized = async function (candidatePassword) {
        try {
            return await bcrypt.compare(candidatePassword, this.password);
        } catch (e) {
            return e;
        }

    };

    User.beforeCreate = async (user) => {
        const salt = await bcrypt.genSalt(10);
        if (user.birthDate) {
            user.birthDate = new Date(user.birthDate);
        }
        user.activationToken = randToken.generate(32);
        if (!user.password) {
            throw ({
                stack: 'PASSWORD_REQUIRED',
                status: 400
            });
        }
        user.password = await bcrypt.hash(user.password, salt);
        return user;
    };

    User.beforeUpdate = async (user) => {
        const salt = await bcrypt.genSalt(10);
        delete user.activationToken;
        delete user.active;
        delete user.admin;
        delete user.email;
        delete user.privacy;
        delete user.termsAndConditions;
        if (user.birthDate) {
            user.birthDate = new Date(user.birthDate);
        }
        if (user.password) {
            user.password = await bcrypt.hash(user.password, salt);
        }
        return user;
    };

    User.verifyToken = async (token) => {
        try {
            return await jwt.verify(token, config.tokenSecret);
        } catch (e) {
            throw e;
        }
    };
    return User;
};

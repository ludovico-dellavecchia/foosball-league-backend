'use strict';
module.exports = (sequelize, DataTypes) => {
    return sequelize.define('game', {
        status: {
            type: DataTypes.ENUM(['pending', 'open', 'close']),
            allowNull: false,
            defaultValue: 'pending'
        }
        }, {
            tableName: 'games',
            paranoid: true
        }
    );
};

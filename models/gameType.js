'use strict';
module.exports = (sequelize, DataTypes) => {
    return sequelize.define('gameType', {
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            description: {
                type: DataTypes.TEXT
            }
        }, {
            tableName: 'gameType',
            paranoid: true
        }
    );
};

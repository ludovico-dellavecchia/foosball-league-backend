'use strict';
module.exports = (sequelize, DataTypes) => {
    return sequelize.define('gameLog', {
            partial: {
                type: DataTypes.INTEGER,
                min: 1,
                max: 3,
                allowNull: false,
                defaultValue: 1
            },
            pointsRed1: {
                type: DataTypes.FLOAT,
                allowNull: false,
                defaultValue: 0,
                comment: 'Point Team Red Player 1'
            },
            pointsRed2: {
                type: DataTypes.FLOAT,
                allowNull: false,
                defaultValue: 0,
                comment: 'Point Team Red Player 2'
            },
            pointsBlue1: {
                type: DataTypes.FLOAT,
                allowNull: false,
                defaultValue: 0,
                comment: 'Point Team Blue Player 3'
            },
            pointsBlue2: {
                type: DataTypes.FLOAT,
                allowNull: false,
                defaultValue: 0,
                comment: 'Point Team Blue Player 4'
            },
            winner: {
                type: DataTypes.ENUM(['Red', 'Blue']),
                allowNull: false
            }
        }, {
            indexes: [
                {
                    name: 'id',
                    unique: true,
                    fields: ['gameId', 'partial']
                }
            ],
            tableName: 'games_log',
            paranoid: true
        }
    );
};

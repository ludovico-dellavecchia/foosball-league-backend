"use strict";
const authorization = require('../../controllers/v1/authorization.controller'),
    Error = require('../../classes/error.class'),
    Invitation = require('../../controllers/v1/invitation.controller'),
    User = require('../../controllers/v1/user.controller');

module.exports = (api) => {
    /**
     * @api {get} /profile Profile
     * @apiName GetUserProfile
     * @apiVersion 1.0.0
     * @apiDescription Get logged user profile info
     * @apiGroup User
     * @apiPermission none
     *
     * @apiSuccess {Object} User Object representing user
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "admin": "false",
     *       "birthDate": "11/08/2016",
     *       "birthPlace": "Torino",
     *       "fiscalCode": "DLLdVE34M1326L219W",
     *       "email": "ludovico.dellavecchia@gmail.com",
     *       "firstName": "John",
     *       "lastName": "Doe",
     *       "fullName": "John Doe",
     *       "phone": "011 819 3009",
     *       "sex": "man",
     *       "smartPhone": "348 275 2657",
     *       "vatNumber": ""
     *     }
     *
     * @apiError Unauthorized User not authorized
     */
    api.route('/profile')
        .get(authorization.requireLogin, (req, res) => {
            res.json(req.user);
        });

    api.route('/user/:id/game')
        .get(authorization.requireLogin, async (req, res) => {
            try {
                res.json([]);
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        });

    api.route('/user/:id/invitation/:invitationId?')
        .get(authorization.requireLogin, async (req, res) => {
            try {
                res.json([]);
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        })
        .post(authorization.requireLogin, async (req, res) => {
            try {
                req.body.invitation.fromUser = req.user.id;
                await Invitation.insert(req.body.invitation);
                res.sendStatus(201);
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        })
        .put(authorization.requireLogin, async (req, res) => {
            try {
                await Invitation.update(req.body.invitation);
                res.sendStatus(201);
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        });

    api.route('/user/:id/stat')
        .get(authorization.requireLogin, async (req, res) => {
            try {
                res.json([]);
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        });

    api.route('/users/:id?')
    /**
     * @api {get} /users/:id? Get User
     * @apiName GetUser
     * @apiVersion 1.0.0
     * @apiDescription Get one or more users
     * @apiGroup User
     * @apiPermission admin
     *
     * @apiParam {Date} birthDate
     * @apiParam {Date} birthPlace
     * @apiParam {String} fiscalCode
     * @apiParam {String} email
     * @apiParam {String} firstName
     * @apiParam {String} lastName
     * @apiParam {String} phone
     * @apiParam {String} sex
     * @apiParam {String} smartPhone
     * @apiParam {String} vatNumber
     *
     * @apiSuccess {Object} User Object representing the user
     * @apiSuccess {Object[]} Users Collection of user
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "admin": "false",
     *       "birthDate": "11/08/2016",
     *       "birthPlace": "Torino",
     *       "fiscalCode": "DLLdVE34M1326L219W",
     *       "email": "ludovico.dellavecchia@gmail.com",
     *       "firstName": "John",
     *       "lastName": "Doe",
     *       "fullName": "John Doe",
     *       "phone": "011 819 3009",
     *       "sex": "man",
     *       "smartPhone": "348 275 2657",
     *       "vatNumber": ""
     *     }
     *
     * @apiError NotFound User not found
     * @apiError Unauthorized User not authorized
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 USER_NOT_FOUND
     */
        .get(authorization.requireLogin, authorization.requireAdmin, async (req, res) => {
            try {
                res.json(await User.get(req));
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        })
        /**
         * @api {post} /users/:id? Post User
         * @apiName PostUser
         * @apiVersion 1.0.0
         * @apiDescription Create user
         * @apiGroup User
         * @apiPermission admin
         *
         * @apiParam {Date} birthDate
         * @apiParam {Date} birthPlace
         * @apiParam {String{..16}} fiscalCode
         * @apiParam {String} email
         * @apiParam {String} firstName
         * @apiParam {String} lastName
         * @apiParam {String} password
         * @apiParam {String} [phone]
         * @apiParam {String="man", "woman", "other"} sex
         * @apiParam {String} [smartPhone]
         * @apiParam {String{..11}} [vatNumber]
         * @apiParam {Boolean} privacy
         * @apiParam {Boolean} termsAndConditions
         *
         *
         * @apiParamExample {json} Request body example
         *
         *  {
         *     "user": {
         *         "firstName": "Ludovico",
         *         "lastName": "Dellavecchia",
         *         "email": "ludovico.dellavecchia+2@gmail.com",
         *         "password": "nonlaso",
         *         "birthDate": "1993/08/11",
         *         "birthPlace": "Torino",
         *         "fiscalCode": "DLLLVC93M11L219W",
         *         "phone": "",
         *         "smartphone": "3482752657",
         *         "termsAndConditions": true,
         *         "privacy": true,
         *         "sex": "man",
         *         "vatNumber": ""
         *     }
         *  }
         *
         * @apiSuccess Created
         *
         * @apiError Unauthorized User not authorized
         * @apiError ValidationError Validation error
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 400
         *     email must be unique
         */
        .post(authorization.requireLogin, authorization.requireAdmin, async (req, res) => {
            try {
                await User.insert(req.body.user);
                res.sendStatus(201);
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        })
        /**
         * @api {post} /users/:id? Update user
         * @apiName UpdateUser
         * @apiVersion 1.0.0
         * @apiDescription Update user (if not admin id is not required)
         * @apiGroup User
         * @apiPermission authenticated
         *
         * @apiParam {Date} birthDate
         * @apiParam {Date} birthPlace
         * @apiParam {String{..16}} fiscalCode
         * @apiParam {String} firstName
         * @apiParam {String} lastName
         * @apiParam {String} password
         * @apiParam {String} [phone]
         * @apiParam {String="man", "woman", "other"} sex
         * @apiParam {String} [smartPhone]
         * @apiParam {String{..11}} [vatNumber]
         *
         * @apiParamExample {json} Request body example
         *   {
         *     "user": {
         *       "firstName": "Ludovico",
         *       "lastName": "Dellavecchia",
         *       "password": "nonlaso",
         *       "birthDate": "Wed Aug 11 1993 00:00:00 GMT+0200 (ora legale Europa occidentale)",
         *       "birthPlace": "Torino",
         *       "fiscalCode": "DLLDVE34M1326L219W",
         *       "phone": "",
         *       "smartphone": "3482752657",
         *       "sex": "man",
         *       "vatNumber": "",
         *     }
         *   }
         *
         * @apiSuccess {String} Modified
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *     Modified
         *
         * @apiError ValidationError generic malformed field or unique constrains
         */
        .put(authorization.requireLogin, async (req, res) => {
            try {
                await User.update(req);
                res.send('Modified');
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }

        })
        /**
         * @api {delete} /users/:id? Delete User
         * @apiName DeleteUser
         * @apiVersion 1.0.0
         * @apiDescription Delete user
         * @apiGroup User
         * @apiPermission admin
         *
         * @apiParam {Integer} id Optional if not admin
         *
         * @apiSuccess Deleted
         *
         * @apiError Unauthorized User not authorized
         * @apiError NotFound User not found
         */
        .delete(authorization.requireLogin, async (req, res) => {
            try {
                await User.delete(req);
                res.send('Deleted');
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        });

    api.route('/user/:id/subscription')
    /**
     * @api {get} /user/:id/subscription Get User Subscriptions
     * @apiName GetUserSubscription
     * @apiVersion 1.0.0
     * @apiDescription Get one or more user subscriptions
     * @apiGroup Subscription
     * @apiPermission admin for all / user for himself
     *
     * @apiParam {Integer} id user id
     *
     * @apiSuccess {Object[]} Subscriptions
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        "id": 8,
     *         "expiringDate": "2017-07-10T14:59:45.000Z",
     *         "createdAt": "2017-07-10T16:42:24.000Z",
     *         "updatedAt": "2017-07-10T16:42:24.000Z",
     *         "deletedAt": null,
     *         "userId": 1,
     *         "productId": 1
     *     }
     *
     * @apiError NotFound User not found
     * @apiError Unauthorized User not authorized
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 USER_NOT_FOUND
     */
        .get(authorization.requireLogin, async (req, res) => {
            try {
                res.json(await User.getSubscription(req));
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        })
        /**
         * @api {post} /user/:id/subscription Post User Subscription
         * @apiName PostUserSubscription
         * @apiVersion 1.0.0
         * @apiDescription Create user subscription
         * @apiGroup Subscription
         * @apiPermission admin
         *

         * @apiParam {Integer} productId
         * @apiParam {Date} expiringDate
         *
         * @apiSuccess Created
         *
         * @apiParamExample {json} Request body example
         *   {
         *      "subscription": {
         *          "productId": 1,
         *          "expiringDate": "Mon Jul 10 2017 16:59:45 GMT+0200 (ora legale Europa occidentale)"
         *      }
         *   }
         *
         * @apiError Unauthorized User not authorized
         * @apiError ValidationError Validation error
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 400
         *     email must be unique
         */
        .post(authorization.requireLogin, authorization.requireAdmin, async (req, res) => {
            try {
                await User.setSubscription(req);
                res.sendStatus(201);
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        })
        /**
         * @api {delete} /user/:id/subscription Delete User Subscription
         * @apiName DeleteUserSubscription
         * @apiVersion 1.0.0
         * @apiDescription Delete user subscription
         * @apiGroup Subscription
         * @apiPermission admin
         *
         * @apiParam {Integer} id user id
         *
         * @apiSuccess Deleted
         *
         * @apiError Unauthorized User not authorized
         * @apiError NotFound User not found
         */
        .delete(authorization.requireLogin, authorization.requireAdmin, async (req, res) => {
            try {
                await User.removeSubscription(req);
                res.end('Deleted');
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        });

    api.route('/user/:id/address')
    /**
     * @api {get} /user/:id/address Get User Addresses
     * @apiName GetUserAddress
     * @apiVersion 1.0.0
     * @apiDescription Get one or more user addresses
     * @apiGroup Address
     * @apiPermission admin for all / user for himself
     *
     * @apiParam {Integer} id user id
     *
     * @apiSuccess {Object[]} Addresses
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *         "route": "Via Martiri della Libertà",
     *         "streetNumber": 2,
     *         "city": "Torino",
     *         "lat": 45.063766,
     *         "lng": 7.703529,
     *         "postalCode": "10131",
     *         "province": "TO",
     *         "state": "Piemonte",
     *         "country": "Italia",
     *         "type": "home",
     *         "committee": "",
     *         "otherSociety": "",
     *         "billingAddress": true
     *     }
     *
     * @apiError NotFound User not found
     * @apiError Unauthorized User not authorized
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 USER_NOT_FOUND
     */
        .get(authorization.requireLogin, async (req, res) => {
            try {
                res.json(await User.getAddress(req));
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        })
        /**
         * @api {post} /user/:id/address Post User Address
         * @apiName PostUserAddress
         * @apiVersion 1.0.0
         * @apiDescription Create user address
         * @apiGroup Address
         * @apiPermission admin
         *

         * @apiParam route
         * @apiParam streetNumber
         * @apiParam city
         * @apiParam lat
         * @apiParam lng
         * @apiParam postalCode
         * @apiParam province
         * @apiParam state
         * @apiParam country
         * @apiParam type
         * @apiParam committee
         * @apiParam otherSociety
         * @apiParam billingAddress
         *
         * @apiSuccess Created
         *
         * @apiParamExample {json} Request body example
         *   {
         *      "address": {
         *          "route": "Via Martiri della Libertà",
         *          "streetNumber": 2,
         *          "city": "Torino",
         *          "lat": 45.063766,
         *          "lng": 7.703529,
         *          "postalCode": "10131",
         *          "province": "TO",
         *          "state": "Piemonte",
         *          "country": "Italia",
         *          "type": "home",
         *          "committee": "",
         *          "otherSociety": "",
         *          "billingAddress": true
         *      }
         *   }
         *
         * @apiError Unauthorized User not authorized
         * @apiError ValidationError Validation error
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 400
         *     email must be unique
         */
        .post(authorization.requireLogin, async (req, res) => {
            try {
                await User.setAddress(req);
                res.sendStatus(201);
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        })
        /**
         * @api {delete} /user/:id/address Delete User Address
         * @apiName DeleteUserAddress
         * @apiVersion 1.0.0
         * @apiDescription Delete user address
         * @apiGroup Address
         * @apiPermission admin
         *
         * @apiParam {Integer} id user id
         *
         * @apiSuccess Deleted
         *
         * @apiError Unauthorized User not authorized
         * @apiError NotFound User not found
         */
        .delete(authorization.requireLogin, async (req, res) => {
            try {
                await User.removeAddress(req);
                res.end('Deleted');
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        });
};
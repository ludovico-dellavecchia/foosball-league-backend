"use strict";
const authorization = require('../../controllers/v1/authorization.controller'),
    Error = require('../../classes/error.class'),
    gameLog = require('../../controllers/v1/gameLog.controller');

module.exports = (api) => {
    api.route('/gameLogs/:id?')
        .get(authorization.requireLogin, async (req, res) => {
            try {
                res.json(await gameLog.get(req));
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        })
        .post(authorization.requireLogin, authorization.requireAdmin, async (req, res) => {
            try {
                //TODO: algoritmo
                await gameLog.insert(req.body.gameLog);
                res.sendStatus(201);
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        })
        .put(authorization.requireLogin, authorization.requireAdmin, async (req, res) => {
            try {
                await gameLog.update(req.body.gameLog);
                res.end('Modified');
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }

        })
        .delete(authorization.requireLogin, authorization.requireAdmin, async (req, res) => {
            try {
                await gameLog.delete(req);
                res.end('Deleted');
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        });
};
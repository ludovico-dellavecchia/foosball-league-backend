"use strict";
const authorization = require('../../controllers/v1/authorization.controller'),
    Error = require('../../classes/error.class'),
    Game = require('../../controllers/v1/game.controller');

module.exports = (api) => {
    api.route('/games/:id?')
        .get(authorization.requireLogin, async (req, res) => {
            try {
                res.json(await Game.get(req));
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        })
        .post(authorization.requireLogin, async (req, res) => {
            try {
                await Game.insert(req.body.game);
                res.sendStatus(201);
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        })
        .put(authorization.requireLogin, async (req, res) => {
            try {
                await Game.update(req.body.game);
                res.end('Modified');
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }

        })
        .delete(authorization.requireLogin, async (req, res) => {
            try {
                await Game.delete(req);
                res.end('Deleted');
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        });
};
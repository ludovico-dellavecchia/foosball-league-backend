const
    authorization = require('../../controllers/v1/authorization.controller'),
    Error = require('../../classes/error.class');

module.exports = (api, passport) => {
    'use strict';
    api.route('/signup')
    /**
     * @api {post} /signup Sign-up
     * @apiName SignUp
     * @apiVersion 1.0.0
     * @apiDescription Sign-up
     * @apiGroup Authentication
     * @apiPermission none
     *
     * @apiParam {Date} birthDate
     * @apiParam {Date} birthPlace
     * @apiParam {String{..16}} fiscalCode
     * @apiParam {String} email
     * @apiParam {String} firstName
     * @apiParam {String} lastName
     * @apiParam {String} password
     * @apiParam {String} [phone]
     * @apiParam {String="man", "woman", "other"} sex
     * @apiParam {String} [smartPhone]
     * @apiParam {String{..11}} [vatNumber]
     * @apiParam {Boolean} privacy
     * @apiParam {Boolean} termsAndConditions
     *
     * @apiParamExample {json} Request body example
     *   {
     *       "firstName": "Ludovico",
     *       "lastName": "Dellavecchia",
     *       "email": "ludovico.dellavecchia@gmail.com",
     *       "password": "nonlaso",
     *       "birthDate": "Wed Aug 11 1993 00:00:00 GMT+0200 (ora legale Europa occidentale)",
     *       "birthPlace": "Torino",
     *       "fiscalCode": "DLLDVE34M1326L219W",
     *       "phone": "",
     *       "smartphone": "3482752657",
     *       "termsAndConditions": true,
     *       "privacy": true,
     *       "sex": "man",
     *       "vatNumber": "",
     *       "userCategoryId": 1
     *   }
     *
     *
     * @apiSuccess {Object} User Object representing the user
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "admin": "false",
     *       "email": "ludovico.dellavecchia@gmail.com"
     *     }
     *
     * @apiError ValidationError generic missing fields or unique constrains
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 400
     *     email must be unique
     */
        .post((req, res, next) => {
            passport.authenticate('local-signup', (err, user) => {
                if (err) {
                    err = new Error(err);
                    return res.status(err.status()).send(err.stack());
                }
                res.json(user);
            })(req, res, next);
        });

    api.route('/login')
    /**
     * @api {post} /login Login
     * @apiName Login
     * @apiVersion 1.0.0
     * @apiDescription Login with header cookie session
     * @apiGroup Authentication
     * @apiPermission none
     *

     * @apiParam {String} email
     * @apiParam {String} password
     *
     * @apiParamExample {json} Request body example
     *   {
     *     "email": "ludovico.dellavecchia@gmail.com",
     *     "password": "nonlaso"
     *   }
     *
     *
     * @apiSuccess {Object} User Object representing the user
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "admin": "false",
     *       "birthDate": "11/08/2016",
     *       "birthPlace": "Torino",
     *       "fiscalCode": "DLLdVE34M1326L219W",
     *       "email": "ludovico.dellavecchia@gmail.com",
     *       "firstName": "John",
     *       "lastName": "Doe",
     *       "fullName": "John Doe",
     *       "phone": "011 819 3009",
     *       "sex": "man",
     *       "smartPhone": "348 275 2657",
     *       "vatNumber": ""
     *     }
     *
     * @apiError Unauthorized Invalid credentials
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401
     *     INVALID_CREDENTIALS
     */
        .post((req, res, next) => {
            passport.authenticate('local-login', (err, user) => {
                if (err) {
                    err = new Error(err);
                    return res.status(err.status()).send(err.stack());
                }
                if (!user) {
                    err = new Error({
                        stack: 'BAD_REQUEST',
                        status: 403
                    });
                    return res.status(err.status()).send(err.stack());
                }
                res.json(user);
            })(req, res, next);
        });

    /**
     * @api {get} /log-out Logout
     * @apiName Logout
     * @apiVersion 1.0.0
     * @apiDescription Logout (clear login session)
     * @apiGroup Authentication
     * @apiPermission none
     */
    api.route('/log-out')
        .get((req, res) => {
            req.logOut();
            res.end();
        });

    /**
     * @api {get} /confirm-email/:activationToken Confirm email
     * @apiName Confirm email
     * @apiVersion 1.0.0
     * @apiDescription Confirm email with token in the sign-up email
     * @apiGroup Authentication
     * @apiPermission none
     *
     * @apiParam {String} activationToken activation token received in the sign up email
     */
    api.route('/confirm-email/:activationToken')
        .get(async (req, res) => {
            try {
                await authorization.confirmEmail(req);
                res.sendStatus(200);
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        });


    /**
     * @api {post} /update-email/:token? Update email
     * @apiName UpdateEmail
     * @apiVersion 1.0.0
     * @apiDescription Set new user email address
     * @apiGroup Authentication
     * @apiPermission authenticated
     *
     * @apiParam {String} token Optional token to confirm email
     * @apiParam {String} email
     * @apiParam {String} password
     *
     * @apiParamExample {json} Request body example
     *   {
     *       "email": "ludovico.dellavecchia@gmail.com",
     *       "password": "nonlaso"
     *   }
     *
     *
     * @apiSuccess 200 OK
     *
     * @apiError Forbidden invalid password
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 403
     *     Forbidden
     */
    api.route('/update-email/:token?')
        .post(authorization.requireLogin, async (req, res) => {
            try {
                await authorization.updateEmail(req);
                res.sendStatus(200);
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        });

    /**
     * @api {post} /reset-password Reset password
     * @apiName Reset password
     * @apiVersion 1.0.0
     * @apiDescription Reset password, send email into body, receive token in email, repeat call with token and new password
     * @apiGroup Authentication
     * @apiPermission none
     *
     * @apiParam {String} email send token to this email
     * @apiParam {String} token get token in the email and repeat the call with also the password
     * @apiParam {String} password get token in the email and repeat the call with also the password
     *
     * @apiParamExample {json} Step 1
     *   {
     *       "email": "ludovico.dellavecchia@gmail.com"
     *   }
     *
     * @apiParamExample {json} Step 2
     *   {
     *       "password": "nonlaso"
     *   }
     *
     * @apiSuccess 200 OK
     *
     * @apiError Forbidden invalid token
     * @apiError MalformedRequest missing new password or email
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 403
     *     Forbidden
     */
    api.route('/reset-password/:token?')
        .post(async (req, res) => {
            try {
                await authorization.resetPassword(req);
                res.sendStatus(200);
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        });

};
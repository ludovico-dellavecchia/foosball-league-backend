"use strict";
const authorization = require('../../controllers/v1/authorization.controller'),
    Error = require('../../classes/error.class'),
    Stat = require('../../controllers/v1/stat.controller');

module.exports = (api) => {
    api.route('/stats/:id?')
        .get(authorization.requireLogin, async (req, res) => {
            try {
                res.json(await Stat.get(req));
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        })
        .post(authorization.requireLogin, authorization.requireAdmin, async (req, res) => {
            try {
                await Stat.insert(req.body.stat);
                res.sendStatus(201);
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        })
        .put(authorization.requireLogin, authorization.requireAdmin, async (req, res) => {
            try {
                await Stat.update(req.body.stat);
                res.end('Modified');
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }

        })
        .delete(authorization.requireLogin, authorization.requireAdmin, async (req, res) => {
            try {
                await Stat.delete(req);
                res.end('Deleted');
            } catch (e) {
                e = new Error(e);
                res.status(e.status()).send(e.stack());
            }
        });
};
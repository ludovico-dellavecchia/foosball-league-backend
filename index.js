const
    express = require('express'),
    app = express(),
    api = express.Router(),
    redisClient = require('redis').createClient(),
    limiter = require('express-limiter')(app, redisClient),
    session = require('express-session'),
    fs = require('fs'),
    db = require('./models/'),
    morgan = require('morgan'),
    path = require('path'),
    json2xls = require('json2xls'),
    csrf = require('csurf'),
    passport = require('passport'),
    cookieParser = require('cookie-parser'),
    cors = require('cors'),
    Store = require('express-sequelize-session')(session.Store),
    bodyParser = require('body-parser'),
    helmet = require('helmet'),
    http = require('http').Server(app),
    config = require('./config/server.config'),
    logStream = fs.createWriteStream(path.join(config.logsPath, 'access.log'), {flags: 'a'});

app.use(morgan('combined', {stream: logStream}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(cookieParser()); // read cookies (needed for auth)
app.use(json2xls.middleware);

// Initialize Passport. Also use passport.session() middleware, to support
// persistent login sessions.
app.use(session({
    name: 'x-secure-foosball',
    secret: config.sessionSecret,
    key: config.segret,
    resave: false,
    saveUninitialized: false,
    store: new Store(db.sequelize),
    cookie: {
        secure: true,
        onlyHttp: true,
        maxAge: 1000 * 60 * 60 * 24 //24hour
    }
}));
app.use(passport.initialize());
app.use(passport.session());
require('./controllers/v1/passport.controller')(passport); // pass passport for configuration


// Add Helmet to Set Sane Defaults
app.use(helmet());

// Block Cross-Site Request Forgeries
app.use(csrf());
app.use(function(req, res, next){
    // Expose variable to templates via locals
    res.locals.csrftoken = req.csrfToken();
    next();
});

//Default server response
app.get('/', (req, res) => {
    res.end();
});

// Limit requests to 100 per hour per ip address.
limiter({
    lookup: ['connection.remoteAddress'],
    total: 100,
    expire: 1000 * 60 * 60
});

//Setup API
const
    whitelist = config.whitelist,
    stream = fs.createWriteStream(path.join(config.logsPath, 'cors.log'), {flags: 'a'}),
    corsOptions = {
        credentials: true,
        origin: async (origin, callback) => {
            if (whitelist.indexOf(origin) !== -1 || origin === undefined) {
                callback(null, true);
            } else {
                let error = {
                    error: origin ? origin : 'undefined origin is not allowed by CORS',
                    date: new Date()
                };
                error = JSON.stringify(error);
                error = error.concat('\n');
                await stream.write(error);
                callback(new Error(origin + ' origin is not allowed by CORS'));
            }
        }
    };
if(whitelist){
    api.use(cors(corsOptions));
}
// api ======================================================================
fs.readdir('./' + config.apiVersion, (err, files) => {
    files.forEach(file => {
        require('./' + config.apiVersion + '/' + file)(api, passport);
    });
});
app.use('/' + config.apiVersion, api);
//Start database
db.sequelize.sync({
    force: true,
    alter: false
}).then(() => {
    //Start server
    http.listen(config.port, () => {
        console.log(`${config.appName} server listening on ${config.endPoint}`);
    });
}).catch((err) => {
    console.error('Unable to connect to the database:', err);
});
module.exports = {
    /**
     * Application configuration section
     * http://pm2.keymetrics.io/docs/usage/application-declaration/
     */
    apps: [
        {
            name: 'foosball-league',
            script: 'index.js',
            watch: false,
            env_staging: {
                NODE_ENV: 'staging',
                PORT: 6060
            },
            env_dev: {
                NODE_ENV: 'dev',
                PORT: 6060
            },
            env_production: {
                NODE_ENV: 'production',
                PORT: 6060
            }
        }
    ]
};
